<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class RoleAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if (!Auth::check()) {
            return view('login');
        } elseif (Auth::user()->level == 1) {
            return $next($request);
        } else {
            return redirect()->route('users.index');
        }
    }
}
