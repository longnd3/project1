<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return view('login');
        } elseif (Auth::user()->level == 0) {
            return $next($request);
        } else {
            return redirect()->route('employee.index');
        }
        
    }
}
