<?php

namespace App\Http\Controllers\Employee;

use Auth;
use Illuminate\Http\Request;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Room\RoomRepositoryInterface;

class EmployeeController extends Controller
{
     /**
        repository user/group/room
    */
    protected $user;
    protected $room;



    public function __construct(UserRepositoryInterface $user, RoomRepositoryInterface $room)
    {
        $this->user = $user;
        $this->room = $room;


    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	return view('users.show');
    }

    public function manage()
    {
        $users = $this->user->getUsersByIdRoom(Auth::user()->room_id);
        return view('rooms.show', ['users' => $users]);
    }

    public function excel($id)
    {
        if (Auth::user()->room_level == 2 || Auth::user()->room_id == $id ) {
            return Excel::download(new UsersExport, 'users.xlsx');
        } else {
            return redirect()->back();          
        }

    }
}
