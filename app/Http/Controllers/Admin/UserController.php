<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Room\RoomRepositoryInterface;


class UserController extends Controller
{
    /**
        repository user/group/room
    */
    protected $user;
    protected $room;



    public function __construct(UserRepositoryInterface $user, RoomRepositoryInterface $room)
    {
        $this->user = $user;
        $this->room = $room;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->user->getDataUsers();
        return view('users.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rooms = $this->room->getDataRooms();
        return view('users.create', ['rooms' => $rooms]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $req)
    {
        $this->user->saveUser($req);
        return redirect()->route('users.index')->with('success', 'Thêm nhân viên thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::id() != $id && Auth::user()->level != 0) {
            return redirect()->back();
        } else {
            $rooms = $this->room->getDataRooms();
            $user = $this->user->getUserById($id);
            return view('users.edit',['user' => $user, 'rooms' => $rooms]); 
        }
            
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $req, $id)
    {
        $this->user->updateUser($req,$id);
        return redirect()->route('users.index')->with('success', 'Sửa thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->user->deleteUser($id);
        return redirect()->route('users.index')->with('success', 'Xóa thành công');        
    }

    /**
     * Reset password user
     *
     * @return \Illuminate\Http\Response
     */

    public function resetPass(Request $req){
        if (is_null($req->checked)) {
            return redirect()->back()
                             ->with('error', 'Không có user nào được chọn để reset password');
        } else {    
            $this->user->resetPass($req);
            return redirect()->route('users.index')
                             ->with('success', 'Reset mật khẩu thành công');
        }

    }
}
