<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePassword;



class LoginController extends Controller
{
	public function getLogin(Request $req) 
	{
		if (!Auth::check()) {
			return view('login');
		} elseif (Auth::user()->is_new == 0) {
			return redirect()->route('password.show', Auth::user()->id);
		} else {
			return redirect()->route('users.index');
		}
	}

	public function postLogin(LoginRequest $req)
	{
		$username = $req->username;
		$password = $req->password;

		if ( Auth::attempt(['username' => $username, 'password' => $password])) {
			if (Auth::user()->is_new == 0) {
				return redirect()->route('password.show', Auth::user()->id);
			} else {
				return redirect()->route('users.index');
			}
			
		} else {
			$errors = new MessageBag(['errorlogin' => 'Username hoặc mật khẩu không đúng']);
			return redirect()->back()->withInput()->withErrors($errors);
		}		
	}

    public function getLogOut() 
    {
		Auth::logout();
		return redirect()->route('login.index');
    }

    public function getResetPassword($id)
    {
    	if (Auth::id() != $id){
    		return redirect()->back();
    	} else {
    		return view('users.reset');
    	}
    	
    }

    public function postResetPassword(ChangePassword $request, $id)
    {
    	$password = bcrypt($request->password);

    	$update_data = [
    		'is_new' => 1,
    		'password' => $password
    	];
    	User::find($id)->update($update_data);
    	if (Auth::user()->level == 0) {
    		return redirect()->route('users.index')->with('success','Mật khẩu của bạn đã được thay đổi');
    	} else {
    		return redirect()->route('employee.index')->with('success','Mật khẩu của bạn đã được thay đổi');
    	}
    	
    }
}
