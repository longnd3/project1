<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = request();
        $id = $request->id;
        if ($request->getMethod() == 'POST') {
            return [
                'name' => 'required|max:255',
                'username' => 'required|unique:users,username',
                'phone' => 'required|digits_between:9,13|unique:infomations,phone',
                'avatar' => 'image',
                'room_id' => 'required',
                'room_level' => 'required',
                'address' => 'required',
                'email' => 'required|email|unique:infomations,email',
            ];            
        } else {
            return [
                'name' => 'required|max:255',
                'phone' => 'required|digits_between:9,13|
                            unique:infomations,phone,' . $id . ',user_id',
                'avatar' => 'image',
                'address' => 'required',
                'email' => 'required|email|unique:infomations,email,' . $id . ',user_id',
            ];     
        }

    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Tên không được để trống!',
            'name.max' => 'Tên tối đa 255 kí tự!',
            'avatar.image' => 'Ảnh phải có đuôi .jpg , .png',
            'username.unique'  => 'Tên đăng nhập đã tồn tại!',
            'username.required'  => 'Tên đăng nhập không được để trống',
            'phone.required'  => 'Số điện thoại không được để trống',
            'phone.digits_between'  => 'Số điện thoại có 9 đến 13 số',
            'phone.unique'  => 'Số điện thoại đã tồn tại',
            'room_id.required'  => 'Chọn 1 phòng ban',
            'room_level.required'  => 'Chọn chức vụ',
            'email.unique' => 'Email đã tồn tại trong hệ thống',
            'email.required' => 'Email không được để trống',
            'email.email' => 'Email có định dạng @gmail.com',


        ];
    }
}
