<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = request();
        $id = $request->id;
        if ($request->getMethod() == 'POST') {
            return [
                'name' => 'required|unique:rooms,name|max:255',
                'manager_id' => 'required',
            ]; 
        } else {
            return [
                'name' => 'required|unique:rooms,name,' .$id. ',id|max:255',
                'manager_id' => 'required',
            ];             
        }           
    }

    public function messages()
    {
        $messages = [
            'name.required' => 'Tên là trường bắt buộc',
            'manager_id.required' => 'Chọn 1 quản lý phòng',
            'name.unique' => 'Tên phòng đã trùng',
        ]; 
        
        return $messages;  
    }
}
