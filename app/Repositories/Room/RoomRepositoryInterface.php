<?php
namespace App\Repositories\Room;

interface RoomRepositoryInterface
{
    public function getDataRooms();

    public function saveRoom($data);

    public function getRoomById($id);

    public function updateRoom($data, $id);

    public function deleteRoom($id);
}