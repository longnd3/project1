<?php
namespace App\Repositories\Room;

use App\Models\Room;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Repositories\EloquentRepository;



class RoomEloquentRepository extends EloquentRepository implements RoomRepositoryInterface
{

    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return \App\Models\Room::class;
    }


    public function getDataRooms()
    {   
        return $this->_model->with('information', 'users')->paginate(8);
    }

    public function saveRoom($data)
    {
        $room_info = [
            'name' => $data->name,
            'manager_id' => $data->manager_id,  
        ];
        DB::transaction(function() use ($room_info) {
            $new_room = $this->_model->create($room_info);
            $this->updateUserLevel($new_room);
        });
        
    }

    public function updateUserLevel($room,$old_room = null)
    {
        if (!is_null($old_room)) {            
            User::where('id', $old_room->manager_id)->update(['room_level' => 1]);
        }
        $update_new_manager = [
            'room_level' => 2, 
            'room_id' => $room->id,
        ];
        User::where('id', $room->manager_id)->update($update_new_manager);
    }

    public function getRoomById($id)
    {
        return $this->_model->find($id);
    }

    public function updateRoom($data, $id)
    {
        $old_room = $this->getRoomById($id);
        $room_info = [
            'name' => $data->name,
            'manager_id' => $data->manager_id,  
        ];
        DB::transaction(function() use ($room_info, $id, $old_room) {
            $this->_model->where('id', $id)->update($room_info);
            $update_room = $this->getRoomById($id);
            $this->updateUserLevel($update_room,$old_room);
        });

    }

    public function deleteRoom($id)
    {
        $delete_room = $this->_model->find($id);

        DB::transaction(function() use ($delete_room) {
            foreach ($delete_room->users as $user) {
                $user->update(['room_id' => 0, 'room_level' => 1]);
            }
            $delete_room->delete();
        });        
    }
}