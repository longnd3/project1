<?php
namespace App\Repositories\User;

use File;
use Mail;
use App\Models\User;
use App\Models\Room;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\EloquentRepository;



class UserEloquentRepository extends EloquentRepository implements UserRepositoryInterface
{

    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return User::class;
    }

    //get all data users
    public function getDataUsers()
    {   
        return $this->_model->with('infomation', 'room')->where('username', '<>', 'admin')->paginate(8);
    }

    public function saveUser($data)
    {
        $password = str_random(6);
        $user_data = [
            'username' => $data->username,
            'password' => Hash::make($password),
            'room_id' => $data->room_id,
            'room_level' => $data->room_level,
            'level' => $data->type
        ];
        DB::transaction(function() use ($user_data, $data) {
            $new_user = $this->_model->create($user_data);
            $this->saveInfomation($data,$new_user);
            $this->updateRoomWhenSaveUser($new_user);
            $this->sendMail($new_user, $password);
        });
        

    }

    public function getUserById($id)
    {
        return $this->_model->with('infomation', 'room')->where('id', $id)->first();
    }

    public function updateUser($data, $id)
    {
        $user_data = array();
        if (!is_null($data->room_id)) {
            $user_data = [
                'room_level' => $data->room_level,
                'room_id' => $data->room_id,
            ];
        }

        $old_user = $this->getUserById($id);
        DB::transaction(function() use ($user_data, $data, $old_user, $id) {
            if (count($user_data) != 0) {
                $this->_model->where('id', $id)->update($user_data);
                $user_update = $this->getUserById($id);
                $this->updateRoomWhenEditUser($user_update, $old_user); 
            }
            $this->updateInfomation($data, $old_user);
        });      
    }

    public function saveInfomation($data, $new_user)
    {
        $avatar = $this->saveImage($data);
        $infomation = $new_user->infomation()->create([
            'name' => $data->name,
            'phone' => $data->phone,
            'email' => $data->email,
            'address' => $data->address,
            'gender' => $data->gender,
            'birthday' => $data->birthday,
            'avatar' => $avatar
        ]);
    }

    public function updateInfomation($data, $user)
    {
        $new_avatar = $this->saveImage($data);
        $avatar = ($new_avatar == '') ? $user->infomation->avatar : $new_avatar;
        $infomation = $user->infomation()->update([
            'name' => $data->name,
            'phone' => $data->phone,
            'email' => $data->email,
            'address' => $data->address,
            'gender' => $data->gender,
            'birthday' => $data->birthday,
            'avatar' => $avatar
        ]);
    }

    public function saveImage($data)
    {
        $path = public_path().'/images/avatar';
        File::makeDirectory($path, $mode = 0777, true, true);
        $avatar = '';
        if ($data->hasFile('avatar')) {

            $images = $data->avatar;
            $imagesName = $images->getClientOriginalName();

            $new_name = time().$imagesName;

            $avatar = 'images/avatar/'.$new_name;

            $images->move($path, $avatar);
        }  
        return $avatar;      
    }

    public function updateRoomWhenSaveUser($user)
    {
        if ($user->room_level == 2) {
            $manage_id = $user->room->manager_id;
            if ($manage_id != 0) {
                $this->_model->where('id', $manage_id)->update(['room_level' => 1]);
            }         
            $user->room()->update(['manager_id' => $user->id]);
        }

    }

    public function updateRoomWhenEditUser($user, $old_user)
    {
        if ($user->room_level == 2) {
            $manage_id = $user->room->manager_id;
            if ($manage_id != 0) {
                $this->_model->where('id', $manage_id)->update(['room_level' => 1]);
            }         
            $user->room()->update(['manager_id' => $user->id]);
            if ($old_user->room_id != $user->room_id && $old_user->room_level == 2) {
                $old_user->room()->update(['manager_id' => 0]);
            }

        } else {
            if ($old_user->room_id == $user->room_id && $old_user->room_level == 2) {
                $old_user->room()->update(['manager_id' => 0]);
            }            
        }
    }

    //get users not manager
    public function getDataEmployees()
    {
        return $this->_model->with('infomation', 'room')->where('room_level', '<>', 2)
                ->where('username', '<>', 'admin')->get();
    }

    public function deleteUser($id){
        $delete_user = $this->_model->find($id);
        $room_id = $delete_user->room_id;
        $room_level = $delete_user->room_level;

        DB::transaction(function() use ($delete_user, $room_id, $room_level) { 
            if ($room_level == 2) {
                $delete_user->room()->update(['manager_id' => 0]);
            }           
            $delete_user->infomation()->delete();
            $delete_user->delete();
        });
    }

    public function resetPass($data)
    {
        $password = '123456';
        $user_data = [
            'password' => Hash::make('123456'),
            'is_new' => 0,
        ];
        if (count($data->checked) != 0) { 
            foreach ($data->checked as $id) {
                $data_u = $this->getUserById($id);
                $this->sendMail($data_u, $password, 2);
            }
            $this->_model->whereIn('id', $data->checked)->update($user_data);
        }     
    }

    public function getUsersByIdRoom($id)
    {
        return $this->_model->with('infomation', 'room')->where('room_id', $id)->paginate(8);
    }

    public function sendMail($data, $password, $role = 1)
    {
        $subject = ($role == 1) ? 'Create Account!' : 'Reset Password!';
        Mail::send('mail.mailfb', array('username' => $data->username, 'password' => $password), function($message) use ($data, $subject) {
            $message->to($data->infomation->email, $data->infomation->name)->subject($subject);
        });
    }
}