<?php
namespace App\Repositories\User;

interface UserRepositoryInterface
{
    public function getDataUsers();

    public function saveUser($data);

    public function getUserById($id);

    public function updateUser($data, $id);

    public function getDataEmployees();

    public function deleteUser($id);

    public function resetPass($data);

    public function getUsersByIdRoom($id);


}