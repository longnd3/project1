<?php

namespace App\Exports;

use Auth;
use App\Models\User;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class UsersExport implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	$id = Auth::user()->room_id;
        return User::with('infomation', 'room')->where('room_id', $id)->get();
    }

    public function map($user): array
    {
        return [
            $user->infomation->name,
            '0' . $user->infomation->phone,
            $user->infomation->email,
            $user->infomation->address,
            ($user->infomation->gender == 1) ? 'Nam' : 'Nữ',
            ($user->infomation->birthday),
            $user->room->name,
            ($user->room_level == 1) ? 'Nhân viên' : 'Trường phòng',
        ];
    }

    public function headings(): array
    {
        return [
            'Họ và tên',
            'Số điện thoại',
            'Email',
            'Địa chỉ',
            'Giới tính',
            'Ngày sinh',
            'Phòng ban',
            'Chức vụ',
        ];
    }
}
