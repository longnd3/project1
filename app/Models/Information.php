<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Information extends Model
{
	use SoftDeletes;
	protected $table = "infomations";
	
    protected $fillable = [
        'name', 'phone', 'user_id', 'avatar', 'address', 'gender', 'birthday', 'email'
    ]; 

	protected $dates = ['deleted_at'];

    public function users()
    {
    	return $this->belongsTo(User::class);
    }
}
