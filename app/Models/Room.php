<?php

namespace App\Models;

use App\Models\User;
use App\Models\Information;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name', 'manager_id'
    ];

    protected $dates = ['deleted_at'];
    
    public function users()
    {
    	return $this->hasMany(User::class);
    }

    public function information()
    {
        return $this->hasOne(Information::class, 'user_id', 'manager_id');
    }

}
