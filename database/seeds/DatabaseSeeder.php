<?php

use App\Models\User;
use App\Models\Room;
use App\Models\Information;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'admin',
            'password' => bcrypt('admin'),
            'room_id' => '0',
            'room_level' => '0',
            'level' => '0',
        ]);
        DB::table('infomations')->insert([
        	'name' => 'Admin',
            'phone' => '0917305659',
            'user_id' => '1',
            'avatar' => 'images/avatar/default.jpg',
            'email' => 'motconvit18@gmail.com',
            'address' => 'Viet Nam',
            'gender' => '1',
            'birthday' => '2019-04-11',

        ]);     	
		//create 20 users
		factory(User::class, 20)->create()->each(function ($user) {
		    factory(Information::class, 1)->create(['user_id'=>$user->id]);
		});

		factory(Room::class, 5)->create();

    }
}
