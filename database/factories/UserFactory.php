<?php

use App\Models\User;
use App\Models\Room;
use App\Models\Information;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'username' => $faker->word,
        'room_id' => $faker->numberBetween($min = 1, $max = 5),
        'room_level' => '1',
        'level' => '1',
        'password' => bcrypt('123456'), // password
    ];
});

$factory->define(Information::class, function (Faker $faker) {
    return [
    	'name' => $faker->name,
        'phone' => $faker->e164PhoneNumber,
        'avatar' => 'images/avatar/default.JPG',
        'email' => $faker->freeEmail,
        'address' => $faker->address,
        'gender' => $faker->numberBetween($min = 1, $max = 2),
        'birthday' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'user_id' => function() {
            return factory(App\Models\User::class)->create()->id;
        },
    ];
});


$factory->define(Room::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'manager_id' => '0',
    ];
});
