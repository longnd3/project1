@extends('_layout.layout')
@section('title')
Quản lý thông tin nhân viên
@endsection
@section('content')
    <div class="conten-wrapper">
        <section class="content container-fluid">
        @if (session('success'))
        <div class="alert alert-success">
              <p>{{ session('success') }}</p>
        </div>
        @endif
        @if (session('error'))
        <div class="alert alert-danger">
              <p>{{ session('error') }}</p>
        </div>
        @endif
        <div class="row">
            <h2>Bảng thông tin nhân viên</h2>
            <hr>
            <a href="{{ route("users.create") }}" type="button" class="btn btn-success">Thêm nhân viên</a>
            <hr>
        </div>
        <form action="{{ route('users.reset') }}" method="POST">
            @csrf
            <input type='submit' class="btn btn-danger" value="Đặt lại mật khẩu">
        
        <table id="table1" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>Chọn</th>
                    <th>STT</th>
                    <th>Tên</th>
                    <th>Username</th>
                    <th>Điện thoại</th>
                    <th>Email</th>
                    <th>Địa chỉ</th>
                    <th>Giới tính</th>
                    <th>Ngày sinh</th>
                    <th>Phòng ban</th>
                    <th>Chức vụ</th>                   
                    <th>Sửa</th>
                    <th>Xóa</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($users as $key => $user)
                    <tr>
                        <td><input type="checkbox" name="checked[]" value="{{ $user->id }}"> 
                        </form> </td>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $user->infomation->name }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ '0'.$user->infomation->phone }}</td>
                        <td>{{ $user->infomation->email }}</td>                        
                        <td>{{ $user->infomation->address }}</td>                        
                        <td>{{ ($user->infomation->gender == 1) ? 'Nam' : 'Nữ' }}</td>             
                        <td>{{ $user->infomation->birthday}}</td>                        
                        <td>{{ (is_null($user->room)) ? "Chưa có phòng ban" : $user->room->name }}</td>
                        <td>{{ ($user->room_level == 1) ? 'Nhân viên' : 'Trường phòng' }}</td>
                        <td><a href="{{route('users.edit',$user->id)}}"><span class="fa fa-edit"></span> Sửa</a></td>
                        <td><form action="{{ route('users.destroy', $user->id )}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <input type='submit' onclick="confirm_delete();" class="btn btn-danger" value="Xóa">
                            </form>
                        </td>
                        
                    </tr>  
            	@endforeach

            </tbody>
        </table>    
        {{ $users->links() }}
        </section>
    </div>
@endsection
@section('js')
<script type="text/javascript">
    function confirm_delete()
    {
        if(!confirm("Bạn chắc chắn muốn xóa nhân viên này"))
          event.preventDefault();            
    }

    function confirm_reset()
    {
        if(!confirm("Bạn chắc chắn muốn reset mật khẩu của những nhân viên này"))
          event.preventDefault();            
    }
    
</script>
@endsection

