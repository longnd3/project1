@extends('_layout.layout')
@section('title')
Đăng kí
@endsection
@section('content')
	<div class="conten-wrapper">
		<section class="content container-fluid">
			<div class="container">
				<h2>Thêm thông tin nhân viên</h2>
				<form class="form-horizontal" method="POST" action="{{ route('users.store') }}" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    <div class="form-group">
                        <p style="text-align: center;"><span class="error">* required field</span></p>
                    </div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Tên <span class="error">*</span></label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">Email <span class="error">*</span></label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                        <label for="username" class="col-md-4 control-label">Tên đăng nhập <span class="error">*</span></label>

                        <div class="col-md-6">
                            <input id="username" type="string" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                            @if ($errors->has('username'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>                    
                    <div class="form-group"{{ $errors->has('type') ? ' has-error' : '' }}>
                        <label for="type" class="col-md-4 control-label">Loại tài khoản <span class="error">*</span></label>

                        <div class="col-md-6">
                            <select class="selectpicker form-control" name="type" id="type" required>
                                <option value="0" @if( old('type') == 0) selected @endif >Admin</option>
                                <option value="1" @if( old('type') == 1) selected @endif>Nhân viên</option> 
                            </select>
                            @if ($errors->has('type'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('type') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group" >
                         <label for="avatar" class="col-md-4 control-label">Ảnh đại diện <span class="error">*</span></label>
                        <div class="col-md-6">
                            <input type="file" class="form-control" id="avatar" accept="image/*" name="avatar"  required>
                        </div>

                    </div>
                    <div class="form-group" {{ $errors->has('gender') ? ' has-error' : '' }}>
                        <label for="gender" class="col-md-4 control-label">Giới tính <span class="error">*</span></label>

                        <div class="col-md-6">
                            <select class="selectpicker form-control" name="gender" id="gender" required>
                                <option selected value="">Chọn</option>
                                <option value="1" @if( old('gender') == 1) selected @endif>Nam</option>
                                <option value="2" @if( old('gender') == 2) selected @endif>Nữ</option>
                            </select>
                            @if ($errors->has('gender'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('gender') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                        <label for="birthday" class="col-md-4 control-label">Ngày tháng năm sinh<span class="error">*</span></label>

                        <div class="col-md-6">
                            <input id="birthday" type="date" class="form-control" name="birthday" value="{{ old('birthday') }}" required autofocus>

                            @if ($errors->has('birthday'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('birthday') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>        
                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label for="phone" class="col-md-4 control-label">Số điện thoại <span class="error">*</span></label>

                        <div class="col-md-6">
                            <input id="phone" type="string" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>

                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>  
                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                        <label for="address" class="col-md-4 control-label">Địa chỉ <span class="error">*</span></label>

                        <div class="col-md-6">
                            <input id="address" type="string" class="form-control" name="address" value="{{ old('address') }}" required autofocus>

                            @if ($errors->has('address'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('room_id') ? ' has-error' : '' }}" id="room_id">
                        <label for="room_id" class="col-md-4 control-label">Phòng ban <span class="error">*</span></label>

                        <div class="col-md-6">
                            <select class="selectpicker form-control select-js" name="room_id" >
                                @foreach($rooms as $rooms)
                                <option value="{{ $rooms->id }}" @if (old('room_id') == $rooms->id) selected @endif>{{ $rooms->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('room_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('room_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>  
                    <div class="form-group" {{ $errors->has('room_level') ? ' has-error' : '' }} id="room_level">
                        <label for="room_level" class="col-md-4 control-label">Chức vụ <span class="error">*</span></label>

                        <div class="col-md-6">
                            <select class="selectpicker form-control" name="room_level" >
                                <option selected value="">Chọn</option>
                                <option value="1" @if( old('room_level') == 1) selected @endif>Nhân viên</option>
                                <option value="2" @if( old('room_level') == 2) selected @endif>Trưởng phòng</option>
                            </select>
                            @if ($errors->has('room_level'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('room_level') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>                                             
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Register
                            </button>
							<a href="{{ route("users.index") }}" type="button" class="btn btn-info">Quay lại</a>
                        </div>

                    </div>
                    </form>
                </div>
			</div>
		</section>	
	</div>
@endsection
@section('css')
<style type="text/css">
	h2 {
		text-align: center;
	}
    .error {
        color: red;
    }
</style>
@endsection
@section('js')
<script type="text/javascript">
    $('#room_id').hide();
    $('#room_level').hide();

    $("select[name='type']").on('change',function(){
        var type = $(this).val();
        if(type == 1){ 
            $('#room_id').show();
            $('#room_level').show();
            $('#room_id').prop('required',true);
            $('#room_level').prop('required',true);

        }else{
            $('#room_id').hide();
            $('#room_level').hide();
             $('#room_id').prop('required',false);
            $('#room_level').prop('required',false);
        }
    });

</script>
@endsection
