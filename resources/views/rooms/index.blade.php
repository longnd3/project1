@extends('_layout.layout')
@section('title')
Quản lý thông tin nhân viên
@endsection
@section('content')
    <div class="conten-wrapper">
        <section class="content container-fluid">
        @if (session('success'))
        <div class="alert alert-success">
              <p>{{ session('success') }}</p>
        </div>
        @endif
        <div>
            <h2>Bảng thông tin phòng ban</h2>
            <hr>
            <a href="{{ route("rooms.create") }}" type="button" class="btn btn-info">Thêm phòng ban</a>
            <hr>
        </div>
        <table id="table1" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Tên phòng ban</th>
                    <th>Trưởng phòng</th>
                    <th>Số nhân viên</th>
                    <th>Sửa</th>
                    <th>Xóa</th>
                </tr>
            </thead>
            <tbody>
            	@foreach($rooms as $key => $room)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $room->name }}</td>
                        <td>{{ (is_null($room->information)) ? "Chưa có trưởng phòng" : $room->information->name }}</td>
                        <td>{{ count($room->users) }}</td>                        
                        <td><a href="{{ route('rooms.edit', $room->id) }}"><span class="fa fa-edit"></span> Sửa</a></td>
                        <td><form action="{{ route('rooms.destroy', $room->id) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <input type='submit' onclick="confirm_delete();" class="btn btn-danger" value="Xóa">
                            </form>
                        </td>
                    </tr>  

            	@endforeach

            </tbody>
        </table>     
        {{ $rooms->links() }}
        </section>
    </div>
@endsection
@section('js')
<script type="text/javascript">
    function confirm_delete()
    {
    if(!confirm("Bạn chắc chắn muốn xóa phòng ban này"))
      event.preventDefault();            
    }
</script>
@endsection
