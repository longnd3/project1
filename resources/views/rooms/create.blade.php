@extends('_layout/layout')
@section('title')
Thêm phòng ban
@endsection
@section('content')
	<div class="conten-wrapper">
		<section class="content container-fluid">
			<div class="container">
				<h2>Thêm phòng ban</h2>
				<form class="form-horizontal" method="POST" action="{{ route('rooms.store') }}" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    <div class="form-group">
                        <p style="text-align: center;"><span class="error">* required field</span></p>
                    </div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
						<label for="name" class="col-md-4 control-label">Tên phòng ban <span class="error">*</span></label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    	
                    </div>
                    <div class="form-group{{ $errors->has('manager_id') ? ' has-error' : '' }}" id="manager_id">
                        <label for="manager_id" class="col-md-4 control-label">Chọn trưởng phòng <span class="error">*</span></label>

                        <div class="col-md-6">
                            <select class="selectpicker form-control select-js" name="manager_id" >
                                <option value="">Chọn</option>
                                @foreach($users as $user)  
                                <option value="{{ $user->id }}" @if(old('manager_id') == $user->id) selected @endif>{{ $user->infomation->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('manager_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('manager_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div> 
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Tạo phòng ban
                            </button>
                            <a href="{{ route("rooms.index") }}" type="button" class="btn btn-danger">Quay lại</a>
                        </div>

                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection
@section('css')
<style type="text/css">
    h2 {
        text-align: center;
    }
	.error {
		color: red;
	}
</style>
@endsection
