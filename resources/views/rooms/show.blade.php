@extends('_layout.layout')
@section('title')
Quản lý thông tin nhân viên
@endsection
@section('content')
    <div class="conten-wrapper">
        <section class="content container-fluid">
        @if (session('success'))
        <div class="alert alert-success">
              <p>{{ session('success') }}</p>
        </div>
        @endif
        <div>
            <h2>Phòng : {{ Auth::user()->room->name }}</h2>
            <hr>
            <a href="{{ route('employee.excel', Auth::user()->room->id) }}" type="button" class="btn btn-info">Xuất file Exel</a>
            <hr>
        </div>
        <table id="table1" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Tên</th>
                    <th>Điện thoại</th>
                    <th>Email</th>
                    <th>Địa chỉ</th>
                    <th>Giới tính</th>
                    <th>Ngày sinh</th>
                    <th>Avatar</th>
                    <th>Chức vụ</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $key => $user)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $user->infomation->name }}</td>
                        <td>{{ '0'.$user->infomation->phone }}</td>
                        <td>{{ $user->infomation->email }}</td>                        
                        <td>{{ $user->infomation->address }}</td>                        
                        <td>{{ ($user->infomation->gender == 1) ? 'Nam' : 'Nữ' }}</td>             
                        <td>{{ $user->infomation->birthday}}</td>
                        <td><img src="{{ asset($user->infomation->avatar) }}" class="avatar"></td>
                        <td>{{ ($user->room_level == 1) ? 'Nhân viên' : 'Trường phòng' }}</td>         
                    </tr>  
                @endforeach

            </tbody>
        </table>    
        {{ $users->links() }}
        </section>
    </div>
@endsection
@section('css')
<style type="text/css">
    .avatar {
        width: 150px;
        height: 100px;
    }
</style>
@endsection
@section('js')
<script type="text/javascript">
    function confirm_delete()
    {
    if(!confirm("Bạn chắc chắn muốn xóa phòng ban này"))
      event.preventDefault();            
    }
</script>
@endsection
