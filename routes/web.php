<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(["middleware" => "auth"], function(){

	//Admin Route
	Route::group(["middleware" => "admin"], function(){
	//users
		Route::get('users', 'Admin\UserController@index')->name('users.index');

		Route::get('users/create', 'Admin\UserController@create')->name('users.create');

		Route::get('users/{user}', 'Admin\UserController@show')->name('users.show');

		Route::delete('users/{user}', 'Admin\UserController@destroy')->name('users.destroy');

		Route::post('users', 'Admin\UserController@store')->name('users.store');

		Route::post('reset', 'Admin\UserController@resetPass')->name('users.reset');

	//room
		Route::get('rooms', 'Admin\RoomController@index')->name('rooms.index');

		Route::get('rooms/create', 'Admin\RoomController@create')->name('rooms.create');

		Route::get('rooms/{room}', 'Admin\RoomController@show')->name('rooms.show');

		Route::get('rooms/{room}/edit', 'Admin\RoomController@edit')->name('rooms.edit');

		Route::patch('rooms/{room}', 'Admin\RoomController@update')->name('rooms.update');

		Route::delete('rooms/{room}', 'Admin\RoomController@destroy')->name('rooms.destroy');

		Route::post('rooms', 'Admin\RoomController@store')->name('rooms.store');
	});


//Employee

	Route::group(["middleware" => "employee"], function(){
		

		Route::get('/manage', 'Employee\EmployeeController@manage')->name('employee.manage');

		Route::get('/excel/{id}', 'Employee\EmployeeController@excel')->name('employee.excel');
	});

	
	Route::get('/', 'Employee\EmployeeController@index')->name('employee.index');
		
	Route::get('users/{user}/edit', 'Admin\UserController@edit')->name('users.edit');

	Route::patch('users/{user}', 'Admin\UserController@update')->name('users.update');

	Route::get('/password/reset/{user}','Admin\LoginController@getResetPassword')->name('password.show');

	Route::post('/password/reset/{user}','Admin\LoginController@postResetPassword')->name('password.request');	
});


//login logout

Route::get('/login','Admin\LoginController@getLogin')->name('login.index');

Route::get('/logout','Admin\LoginController@getLogOut')->name('logout');

Route::post('/login','Admin\LoginController@postLogin')->name('login');




